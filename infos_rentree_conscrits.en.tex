% vim: spell spelllang=en et ts=4 sw=4
\title{Students' IT services at ENS}
\author{Klub Dev ENS, \textit{aka} KDE}
\date{School term 2019}


\documentclass[11pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[left=2cm,right=2cm,top=1cm,bottom=2cm]{geometry}
\usepackage{enumitem}

\usepackage{hyperref_settings}
\usepackage{todo}
\renewcommand{\labelitemi}{---}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\mail}[1]{\href{mailto:#1}{\texttt{#1}}}
\newcommand{\serviceTitre}[2]{
    \href{#2}{\textbf{\textit{#1}}}\hfill
    \href{#2}{\texttt{#2}}\\
    }

\begin{document}

\maketitle

This document is meant to inform you on the various IT services
administered by and for ENS students. They will be useful to you both for work
and student life. It also sums up a few things on how IT works at ENS in
general.

\section{Students' computer services at ENS}

At ENS, part of the IT services you will be using are entirely administered by
students. If you ever encounter any problem, have a question or a request about
those, you can send us an email at \mail{klub-dev@ens.fr} or drop by on
\emph{Merle} (see below), team \emph{Klub Dev ENS}.
All the links from below are also on
\href{https://www.eleves.ens.fr}{www.eleves.ens.fr}.

\medskip

\begin{itemize}[itemsep=0.8ex]

    \item \serviceTitre{Students' directory}{https://www.eleves.ens.fr/annuaire/}
        If you never change anything there, your entry will hold your name, ENS
        email address, your promotion and the identity photo you provided to
        ENS, but you can add plenty of things there! Useful once you take part
        a little in the school's life.\\
        Note that these informations are \emph{only} visible by other students.

    \item \serviceTitre{Merle}{https://merle.eleves.ens.fr}
        The students' chat server. Come on \textit{Courô} for all general
        conversations!

    \item \serviceTitre{RezoWeb}{https://www.eleves.ens.fr/rezoweb}
        To connect your computer to the internet with your room's wired
        network.

    \item \serviceTitre{ExperiENS}{https://www.eleves.ens.fr/experiens}
        Archive of the students' internship experiences, to help you find
        yours.

    \item \serviceTitre{NextCloud}{https://cloud.eleves.ens.fr}
        A NextCloud server to host and share your files, sync your calendar,
        etc.\ which, unlike Google or Apple, doesn't sell your data for
        advertising purposes.

    \item \serviceTitre{Pads}{https://www.eleves.ens.fr/pads}
        Collaborative online text editor. Very useful in many situations, may
        it be to write an email collaboratively, organize an event or hold a
        grocery list for your dorm's floor.

    \item \serviceTitre{Calc}{https://www.eleves.ens.fr/calc}
        Same thing as the pads, spreadsheet flavour.

    \item \serviceTitre{Git server}{https://git.eleves.ens.fr}
        A Git server to host the source code of the computer science students
        --~but not only that!

    \item \serviceTitre{Students' wiki}{https://wiki.eleves.ens.fr}
        An internal Wiki for the students, holding plenty of informations.
        Clubs can have a wiki there, private or not. You should definitely have
        a look!

    \item \serviceTitre{Tutors' website}{https://www.tuteurs.ens.fr/}
        Old and obsolete for a good part, but still holds a good deal of useful
        knowledge on IT, at ENS or in general.
\end{itemize}
\newpage{}

\section{General IT at ENS}

\subsection{Passwords}

You have two different passwords, not to be confused:

\begin{itemize}
    \item \textbf{GPS password}, handled by the CRI, to sign in to GPS, the
        ENT, WifiENS and Eduroam;
    \item \textbf{Clipper password}, handled by the SPI, to sign in to your
        mails, the free-access computers and all of the students' services (see
        behind: COF, DG, Merle, \ldots{}).
\end{itemize}

\subsection{Connect to the Internet}

\begin{itemize}
    \item From your room: wired network, using RezoWeb (see behind) to
        register your computer.

    \item At ENS, pretty much everywhere: WiFi,
        \begin{itemize}
            \item Eduroam (recommended): follow the instructions at
                \url{https://www.tuteurs.ens.fr/internet/eduroam.html} (no
                English version available, ask someone for help if you need).\\
                Eduroam is available at ENS, but also in many European
                universities --~and even some railway stations, airports,
                \ldots{}. A bit tiresome to get working, but worth the pain!
            \item WifiENS: doesn't always work very well. The password is
                \texttt{louis pasteur}. Once connected, you will be asked for
                your GPS password (see above).
        \end{itemize}
\end{itemize}

\subsection{Printing and scanning}

You can print your documents from the Infi room (next to the mailboxes and DG,
close to the COF); the Info\,3 and Info\,4 rooms, NIR building, floor
-2; and the computer room of Montrouge, at the top of the C building.
Please don't print 300 posters or a 200 pages book, though.
Note that paper is \emph{not provided}!

\medskip

A scanner is also available in the Infi room. Note that it is
only connected to the computer next to it.

\subsection{Who does what?}

\begin{itemize}
    \item The CRI (offices right behind the security point, main entrance of
        the ENS): all the infrastructure and a good portion of
        the hardware. Takes care of the whole IT of the administration.

    \item The SPI (offices in the CS dept., ground floor, Rataud wing):
        students' emails, the free-access computer rooms
        --~including printers~-- and the IT of the CS and maths
        departments.

    \item The students (Klub Dev ENS): all the services at the front of this
        sheet\footnote{except for NextCloud, handled by the SPI}, the websites
        of the COF, DG and plenty of clubs, \ldots{} If you would like
        some service to be installed, you can contact us!
\end{itemize}

\subsection{Mailing-lists}

\paragraph{Sympa mailing-lists.}
Students' clubs mostly use mailing-lists handled by \textit{sympa}:
\href{https://lists.ens.fr/wws/}{lists.ens.fr}. From this interface, you can
sign up or out of a list, ask a list to be created, \ldots{}

\paragraph{Standard mailing-lists.}

\begin{itemize}
    \item \texttt{tous@ens.fr}: every single person at ENS, administration,
        teachers and staff included.

    \item \texttt{tous@clipper.ens.fr}: every student, but not ENS staff.

    \item \texttt{lettres@clipper.ens.fr}, or some other department: every
        student of a given department.

    \item \texttt{informatique19@clipper.ens.fr}, or other department:
        every student of a given department from a given entrance year.
\end{itemize}


\end{document}
